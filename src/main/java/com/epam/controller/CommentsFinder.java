package com.epam.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

public class CommentsFinder {

    private Scanner in = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger(CommentsFinder.class);

    public void commentsFinder() {
        String fileName;
        do {
            fileName = in.nextLine();
        } while (fileName.contains("[.java]"));
        try (DataInputStream in = new DataInputStream(new BufferedInputStream(new FileInputStream(fileName)))) {
            while (true) {
                String line = in.readLine();
                if (line == null) {
                    break;
                }
                int index = line.indexOf("//");
                if (index != -1) {
                    logger.info(line.substring(index));
                }
            }
        } catch (IOException e) {
            logger.error("IO error");
        }
    }
}
