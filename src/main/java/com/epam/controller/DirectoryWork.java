package com.epam.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.Objects;
import java.util.Properties;

import static com.epam.helper.Const.PROPERTIES_FILE_NAME;

public class DirectoryWork {

    private Properties path;
    private static Logger logger = LogManager.getLogger(DirectoryWork.class);

    public DirectoryWork() {
        path = new Properties();
        try {
            path.load(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream(PROPERTIES_FILE_NAME)));
        } catch (IOException e) {
            logger.error(e.getStackTrace());
        }
    }

    public void showContentsDirectory() {
        File file = new File(path.getProperty("c"));
        if (file.exists()) {
            printDirectory(file, "");
        } else {
            logger.info("Directory do not exists.");
        }
    }

    private void printDirectory(File file, String str) {
        str = str + "  ";
        File[] fileNames = file.listFiles();
        if (!Objects.isNull(fileNames)) {
            for (File f : fileNames) {
                if (f.isDirectory()) {
                    printDirectory(f, str);
                } else {
                    logger.info(str + "File: " + f.getName());
                }
            }
        }
    }
}
