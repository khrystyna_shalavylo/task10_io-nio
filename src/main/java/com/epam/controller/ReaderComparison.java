package com.epam.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.Objects;
import java.util.Properties;

import static com.epam.helper.Const.PROPERTIES_FILE_NAME;

public class ReaderComparison {

    private Properties path;
    private static Logger logger = LogManager.getLogger(ReaderComparison.class);
    private long startTime;
    private long endTime;

    public ReaderComparison() {
        path = new Properties();
        try {
            path.load(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream(PROPERTIES_FILE_NAME)));
        } catch (IOException e) {
            logger.error(e.getStackTrace());
        }
        startTime = 0L;
        endTime = 0L;
    }

    private void usualReader() {
        startTime = System.currentTimeMillis();
        try (InputStream inputstream = new FileInputStream(path.getProperty("reader"))) {
            int data = inputstream.read();
            while (data != -1) {
                data = inputstream.read();
            }
            endTime = System.currentTimeMillis();
        } catch (IOException e) {
            logger.error("Usual reader IO error.");
        }
        logger.info("Usual reader finished in: " + ((endTime - startTime) / 1000) + " seconds.");
    }

    private double bufferedReader10MBFile() {
        startTime = System.currentTimeMillis();
        try (BufferedInputStream in = new BufferedInputStream(new FileInputStream(path.getProperty("reader")))) {
            int[] fileArray = new int[in.available()];
            for (int i = 0, copy = 0; (copy = in.read()) != -1; i++) {
                fileArray[i] = copy;
            }
            endTime = System.currentTimeMillis();
        } catch (FileNotFoundException e) {
            logger.error("File not found error.");
        } catch (IOException e) {
            logger.error("Usual reader IO error.");
        }
        logger.info("Buffered reader finished in: " + ((endTime - startTime) / 1000) + " seconds.");
        return (double) ((endTime - startTime) / 1000);
    }

    private double bufferedReader320MBFile() {
        startTime = System.currentTimeMillis();
        try (BufferedInputStream in = new BufferedInputStream(new FileInputStream(path.getProperty("reader2")))) {
            int[] fileArray = new int[in.available()];
            for (int i = 0, copy = 0; (copy = in.read()) != -1; i++) {
                fileArray[i] = copy;
            }
            endTime = System.currentTimeMillis();
        } catch (FileNotFoundException e) {
            logger.error("File not found error.");
        } catch (IOException e) {
            logger.error("Usual reader IO error.");
        }
        logger.info("Buffered reader for 320 MB file finished in: " + ((endTime - startTime) / 1000) + " seconds.");
        return (double) ((endTime - startTime) / 1000);
    }

    public void readerTime() {
        ReaderComparison r = new ReaderComparison();
        r.usualReader();
        double firstBufferedResult = r.bufferedReader10MBFile();
        double secondBufferedResult = r.bufferedReader320MBFile();
        double result = secondBufferedResult - firstBufferedResult;
        logger.info("Difference between buffered readers for 40MB and 320 MB files: " + result + " seconds.");
    }
}
