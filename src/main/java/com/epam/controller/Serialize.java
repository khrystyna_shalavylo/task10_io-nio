package com.epam.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Objects;
import java.util.Properties;

import static com.epam.helper.Const.*;

public class Serialize {

    private Properties path;
    private static Logger logger = LogManager.getLogger(Serialize.class);

    public Serialize() {
        path = new Properties();
        try {
            path.load(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream(PROPERTIES_FILE_NAME)));
        } catch (IOException e) {
            logger.error(e.getStackTrace());
        }
    }

    public void serialized() {
        SHIP1.setDroid(DROID1);
        SHIP1.setDroid(DROID2);
        SHIP2.setDroid(DROID3);
        SHIP2.setDroid(DROID4);
        SHIP2.setDroid(DROID5);
        try (FileOutputStream droids = new FileOutputStream(path.getProperty("droids"))) {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(droids);
            objectOutputStream.writeObject(SHIP1);
            objectOutputStream.writeObject(SHIP2);
        } catch (IOException e) {
            logger.error("Ships IO error.");
        }
    }
}
