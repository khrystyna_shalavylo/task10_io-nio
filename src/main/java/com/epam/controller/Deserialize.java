package com.epam.controller;

import com.epam.model.Ship;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.Objects;
import java.util.Properties;

import static com.epam.helper.Const.PROPERTIES_FILE_NAME;

public class Deserialize {

    private static Logger logger = LogManager.getLogger(Deserialize.class);
    private Properties path;

    public Deserialize() {
        path = new Properties();
        try {
            path.load(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream(PROPERTIES_FILE_NAME)));
        } catch (IOException e) {
            logger.error(e.getStackTrace());
        }
    }

    public void deserialize() {
        try (FileInputStream droids = new FileInputStream(path.getProperty("droids"))) {
            ObjectInputStream objectInputStream = new ObjectInputStream(droids);
            Ship ship1 = (Ship) objectInputStream.readObject();
            logger.info("Deserialization: " + ship1);
            Ship ship2 = (Ship) objectInputStream.readObject();
            logger.info("Deserialization: " + ship2);
        } catch (ClassNotFoundException e) {
            logger.error("Class not found error.");
        } catch (IOException e) {
            logger.error("IO error.");
        }
    }
}
