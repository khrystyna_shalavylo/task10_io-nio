package com.epam.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.Objects;
import java.util.Scanner;


public class MyInputStream extends InputStream {

    private static Logger logger = LogManager.getLogger(MyInputStream.class);
    private String fileName;
    private char[] n;
    private File file;
    private Scanner in = new Scanner(System.in);

    public MyInputStream() {
        fileName = in.nextLine();
        file = new File(fileName);
        n = new char[(int) file.length()];
    }

    @Override
    public int read() {
        if (!Objects.isNull(file)) {
            try (InputStream input = new FileInputStream(file)) {
                for (int i = 0; i < n.length; i++) {
                    n[i] = (char) input.read();
                    System.out.print(n[i]);
                }
            } catch (IOException e) {
                logger.error("IO error.");
            }
            return 0;
        } else {
            logger.info("File is empty.");
            return -1;
        }
    }

    @Override
    public void close() {
        in.close();
    }
}
