package com.epam.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class SomeBuffer {

    private int flag;
    private List<String> buffer;
    private Properties path;
    private static final String propertiesFileName = "path.properties";
    private static Logger logger = LogManager.getLogger(SomeBuffer.class);

    public SomeBuffer() {
        buffer = new ArrayList<>();
        flag = 0;
        path = new Properties();
        try {
            path.load(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream(propertiesFileName)));
        } catch (IOException e) {
            logger.error(e.getStackTrace());
        }
    }

    public void reader() {
        flag = 0;
        Path readerPath = Paths.get(path.getProperty("buffer"));
        try (BufferedReader reader = Files.newBufferedReader(readerPath, Charset.forName("UTF-8"))) {
            String currentLine = reader.readLine();
            while (!Objects.isNull(currentLine)) {
                buffer.add(currentLine);
                currentLine = reader.readLine();
            }
            logger.info(buffer);
        } catch (IOException e) {
            logger.error("Reader IO error.");
        }
    }

    private void writer(Scanner in) {
        flag = 1;
        String someText = in.nextLine();
        Path writerPath = Paths.get(path.getProperty("buffer"));
        buffer.add(someText);
        try (BufferedWriter writer = Files.newBufferedWriter(writerPath, Charset.forName("UTF-8"))) {
            writer.write(buffer.toString());
            buffer.clear();
        } catch (IOException e) {
            logger.error("Writer IO error.");
        }
    }

    public void flip() {
        if (flag == 0) {
            writer(new Scanner(System.in));
        } else {
            reader();
        }
    }
}
