package com.epam.view;

import com.epam.controller.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {

    private static Logger logger = LogManager.getLogger(View.class);
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public View() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
        menu.put("1", "  1 - Test serialization.");
        menu.put("2", "  2 - Test deserialization.");
        menu.put("3", "  3 - Find all comments from java file (enter path to file).");
        menu.put("4", "  4 - Show contents of directory.");
        menu.put("5", "  5 - Test SomeBuffer.");
        menu.put("6", "  6 - Compare readers.");
        menu.put("7", "  7 - Test my InputStream (enter path to file).");
        menu.put("Q", "  Q - exit");

        methodsMenu.put("1", this::testSerialization);
        methodsMenu.put("2", this::testDeserialization);
        methodsMenu.put("3", this::findComments);
        methodsMenu.put("4", this::showDirectory);
        methodsMenu.put("5", this::testSomeBuffer);
        methodsMenu.put("6", this::compareReaders);
        methodsMenu.put("7", this::testMyInputStream);
        methodsMenu.put("Q", this::exit);
    }

    private void outputMenu() {
        logger.info("Menu:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    private void exit() {
        System.exit(0);
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("Please, select menu point:");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (IOException e) {
                logger.error("IO error.");
            }
        } while (!keyMenu.equals("Q"));
    }

    private void testSerialization() {
        Serialize serialize = new Serialize();
        serialize.serialized();
    }

    private void testDeserialization() {
        Deserialize deserialize = new Deserialize();
        deserialize.deserialize();
    }

    private void findComments() {
        CommentsFinder commentsFinder = new CommentsFinder();
        commentsFinder.commentsFinder();
    }

    private void showDirectory() {
        DirectoryWork directoryWork = new DirectoryWork();
        directoryWork.showContentsDirectory();
    }

    private void testSomeBuffer() {
        SomeBuffer someBuffer = new SomeBuffer();
        someBuffer.reader();
        someBuffer.flip();
        someBuffer.flip();
        someBuffer.flip();
        someBuffer.flip();
    }

    private void compareReaders() {
        ReaderComparison readerComparison = new ReaderComparison();
        readerComparison.readerTime();
    }

    private void testMyInputStream() {
        MyInputStream m = new MyInputStream();
        m.read();
        m.close();
    }
}
