package com.epam.model;

import java.io.Serializable;

public class Droid implements Serializable {

    private static final long serialVersionID = 2L;
    private int droidPower;
    private String droidName;

    public Droid(int droidPower, String droidName) {
        this.droidPower = droidPower;
        this.droidName = droidName;
    }

    public int getPower() {
        return this.droidPower;
    }

    public String getName() {
        return this.droidName;
    }

    public void setPower(int power) {
        this.droidPower = power;
    }

    public void setName(String name) {
        this.droidName = name;
    }

    @Override
    public final String toString() {
        return "{" + droidName + " - " + droidPower + "}";
    }
}
