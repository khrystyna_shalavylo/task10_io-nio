package com.epam.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Ship implements Serializable {

    private static final long serialVersionID = 1L;
    private String shipName;
    private List<Droid> droids;
    private transient int capacity;

    public Ship(int capacity, String shipName) {
        droids = new ArrayList<>();
        this.capacity = capacity;
        this.shipName = shipName;
    }

    public void setName(String name) {
        this.shipName = name;
    }

    public void setDroid(Droid droid) {
        this.droids.add(droid);
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public String getDroids() {
        return this.droids.toString();
    }

    public int getCapacity() {
        return this.capacity;
    }

    public String getName() {
        return this.shipName;
    }

    @Override
    public final String toString() {
        return "{" + shipName + ": " + capacity + " <" + droids + ">}";
    }
}
