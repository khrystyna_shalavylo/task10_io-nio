package com.epam.helper;

import com.epam.model.Droid;
import com.epam.model.Ship;

public class Const {

    public static final String PROPERTIES_FILE_NAME = "path.properties";
    public static final Droid DROID1 = new Droid(4, "Soda");
    public static final Droid DROID2 = new Droid(3, "Mone");
    public static final Droid DROID3 = new Droid(5, "Drogo");
    public static final Droid DROID4 = new Droid(1, "Don");
    public static final Droid DROID5 = new Droid(5, "Khal");
    public static final Ship SHIP1 = new Ship(2, "Tornado");
    public static final Ship SHIP2 = new Ship(3, "BigBoss");
}
